<?php

//Recup model
require_once 'model.php';

$psql = new PSQL;
$dbcon = $psql->getDB();

// If Ajout DB
if (isset($_POST['submit'])) {
    $NomBDD = $_POST['NomBDD'];
    $PrenomBDD = $_POST['PrenomBDD'];
    $resultINSERT = $psql->insert($NomBDD, $PrenomBDD);
}

if(isset($_GET['id'])){
    $id = $_GET['id'];
    $psql->delete($id);
}

//Select dans DB
$tableau = $psql->select();

//Create table si exist pas
if(empty($tableau)){
    $table = $psql->createTable();
    $tableau = $psql->select();
} 

//Affichage vue
require_once 'view.php';


