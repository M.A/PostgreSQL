<?php

class PSQL {

    /**
     * Connection BDD
     * @return \PDO
     */
    public function getDB() {
        $dbcon = new PDO('pgsql:dbname=postgres user=postgres password=toor');
        return $dbcon;
    }
    
    /**
     * CREATE TABLE 
     */
    public function createTable(){
        $dbcon = $this->getDB();
        $query = $dbcon->prepare('CREATE TABLE users (id SERIAL, nom TEXT, prenom TEXT)');
        $query->execute();
    }

    /**
     * SELECT
     * @return type
     */
    public function select() {

        $dbcon = $this->getDB();
        $query = $dbcon->prepare('SELECT * FROM "users";');
        $query->execute();
        $tableau = $query->fetchAll();
        return $tableau;
    }

    /**
     * INSERT 
     * @param type $NomBDD
     * @param type $PrenomBDD
     * @return type
     */
    public function insert($NomBDD, $PrenomBDD) {
        $dbcon = $this->getDB();
        $resultINSERT = $dbcon->prepare('INSERT INTO users (nom, prenom) VALUES (:nom, :prenom) ');
        $resultINSERT->execute(array(
            ':nom' => $NomBDD,
            ':prenom' => $PrenomBDD
        ));
        return $resultINSERT;
    }
    
    /**
     * DELETE 
     * @param type $id
     * @return type
     */
    public function delete($id) {
        $dbcon = $this->getDB();
        $resultDELETE = $dbcon->prepare('DELETE FROM users WHERE id = :id ');
        $resultDELETE->execute(array(':id' => $id));
        return $resultDELETE;
    }

}
